#!/bin/sh
#
# Creates a CVS repository with a single file and a single commit.
#
# Copyright (C) 2012, 2020  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


set -e

# CVS wants $USER, $LOGNAME, and $LOGNAME set to a real username.
export USER=root
export LOGNAME=$USER
export USERNAME=$USER

# create the repository
repo="$DATADIR/cvs-test-repo"
export CVSROOT="$repo"
export CVS_RSH=
cvs init

# create a local working copy
workingcopy="$DATADIR/cvs-test-checkout"
mkdir "$workingcopy"
cd "$workingcopy"
cvs checkout CVSROOT
cd "$workingcopy/CVSROOT"

# ensure that our commit has a later timestamp than cvs init's
# "initial checkin"
sleep 1

# add the test file
echo "first line" > test.txt
cvs -Q add test.txt

# make a commit
cvs -Q commit -m "first commit"

# create the .lorry file for the repository
cat <<EOF > $DATADIR/cvs-test-repo.lorry
{
  "cvs-test-repo": {
    "type": "cvs",
    "url": "$repo",
    "module": "CVSROOT"
  }
}
EOF


# create the working directory
test -d "$DATADIR/work-dir" || mkdir "$DATADIR/work-dir"

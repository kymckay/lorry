#!/bin/bash
#
# Tests update counts in working repositories
#
# Copyright (C) 2012-2013, 2020  Codethink Limited
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


set -e
set -o pipefail

repo_name="update-count-test-repo"
logfile="$DATADIR/$repo_name.log"
workdir="$DATADIR/work-dir"
repo="$DATADIR/$repo_name"

normalize() {
    sed -r -e '/hooks\/.*\.sample/d' \
           -e "s/pack-[0-9a-z]+\.(idx|pack)$/pack-file/" \
           -e "/\/objects\/info\/commit-graph$/d" \
           -e "/\/objects\/pack\/pack-[0-9a-z]+\.bitmap$/d" \
           -e "s|$DATADIR|DATADIR|g" "$@"
}

# update it 3 times, which should write a=1, b=2, a=3
for i in 1 2 3; do
    "${SRCDIR}/test-lorry" --pull-only --log="$logfile" --working-area="$workdir" --bundle=never \
      "$DATADIR/update-count-test-repo.lorry"
done

find "$workdir/update-count-test-repo" | LC_ALL=C sort | normalize
grep -H . "$workdir/update-count-test-repo/git"*"/lorry-update-count" | normalize
